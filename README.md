# Tutorial zur Textdatenverarbeitung

Editionsprojektetreffen 02/2016

## Motivation
ein nicht ganz ernst gemeinstes <!-- .element class="fragment" -->

[Anwendungsbeispiel](https://twitter.com/FartForCongress)

## Ausgangssituation

Wir haben einen Text. Zufällig ist der im TEI/XML-Format gespeichert wurden. Diesen wollen wir uns einmal anschauen.
Gewählt wurde ein Erzähltext aus dem TextGrid Repository, der auch in anderen Fällen als Beispiel herhalten musste: [Johanna Spyri: Heidis Lehr- und Wanderjahre](https://textgridrep.org/browse/-/browse/vqn2_0).

## Überblick
### Language Detection

[Unicode](http://unicode.org/udhr/) stellt die UHDR in vielen Sprachen und mit standardisiertem Encoding zur Verfügung: [UHDR (pain text, zipped)](http://unicode.org/udhr/assemblies/udhr_txt.zip)

Diese wird benutzt, um im R package [koRpus](https://cran.r-project.org/web/packages/koRpus/vignettes/koRpus_vignette.pdf) die Runktion guess.lang() anzusprechen.

```r
> install.packages("koRpus")
> library("koRpus", lib.loc="~/R/x86_64-pc-linux-gnu-library/3.3")
> guess.lang( "~/Documents/textdatenverarbeitung/Beispiel/Freytag-DieAhnen.txt", "~/Documents/textdatenverarbeitung/Language_Detection" )

  Estimated language: German, Standard (1901)
          Identifier: deu
             Country:  (Europe)

429 different languages were checked.
```
Aha. Unser Text ist also auf Deutsch und der Language Detection Algorithmus hat den Test bestanden und liefert und den ISO 639 Code der Sprache mit: 1901 (= Deutsch, alte Rechtschreibung).

### Voyant
Schauen wir uns die Basics einmal an.

#### Types and Tokens
»This corpus has 1 document with 51,302 total words and 5,799 unique word forms.«
[Voyant](http://voyant-tools.org/?corpus=979d847454c2d10b755dc89ad1a7cb9d&view=Summary)

#### Type-Token-Rartio


#### Stopwörter

Funktionswörter - für Inhalt nicht relevant, »unwichtige« Wörter

[Listen via Wikipedia](https://de.wikipedia.org/wiki/Stoppwort)

#### N-Grams
Untersucht man in unmittelbarer Nähe zusammenstehende Wörter, bezeichnet man diese als N-Grams.

Bi-Grams, Tri-Grams, 4-Grams, N-Grams


#### Grenzen

Voyant ist sehr oberflächlich. Hübsch, aber für konkrete Anwednungen (Information Retrieval) nicht zu gebrauchen.

### AntConc

Etwas ausgefeiltere Verfahren bietet (AntConc)[http://www.laurenceanthony.net/software/antconc/]

#### Collocation

Statisstisches Verfahren zur Findung von Nahe beieinander stehenden Wörtern. Liefert auch N-Grams, aber deutlich mehr.

### NLP: DKPro-Wrapper
```bash
java -Xmx8g -jar ddw-0.4.6.jar -language de -input ../../Beispiel/Freytag-DieAhnen.txt -output ../DKPro-Wrapper_Output
INFO: Input: ../../Beispiel/Freytag-DieAhnen.txt
INFO: Output: .
INFO: Config: configs/default.properties, configs/default_de.properties
INFO: Language: de
INFO: Reader: Text
INFO: Process 1 files
INFO: Start running the pipeline (this may take a while)...
INFO: Process file: Freytag-DieAhnen.txt
INFO: ---- DONE -----
INFO: All files processed in 11.55 minutes
```
